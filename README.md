# UWA Browse Basin 2010 Experiment - Data Processing

Scripts for converting the raw instrument data into netcdf format with
suitable metadata. Adds the metadata to an sql database

---

Matt Rayson

University of Western Australia

Jan 2017

---

## Requirements

 - [**mycurrents**](https://bitbucket.org/mrayson/mycurrents): Contains classes
 for parsing different file formats data sets (wraps [**dolfyn**](https://github.com/lkilcher/dolfyn) package.)
 - [**soda**](https://github.com/mrayson/soda): Required by `mycurrents`.

---

This contains a howto for converting raw field data into useful netcdf files

## Step 1: Create a csv file with the file metadata

See the file `SCRIPTS/UWA-BB2010-Instruments.csv` file for the necessary fields

## Step 2: Convert the csv to a db (sql) format

See: `SCRIPTS/convert_instrument_csv2sql.py`

(These first two steps may hopefully eventually become redundant when/if we use
an IMOS-like deployment database)

## Step 3: Convert the seabird data to netcdf

See: `SCRIPTS/parse_seabird.py`

TODO: Write parser for .cap format (SBE37-CT)

## Step 4: Convert the raw RDI adcp data to netcdf

See: `SCRIPTS/parse_adcp.py`

**Note**: cannot read 150 kHz file with dolfyn

## Step 5: Process the raw ADV data

TODO: Read ADV data and write to netcdf (currently saves as hdf)

A test script is here: `SCRIPTS/test_read_adv.py`

## Step 6: Create an sql database of the "raw" netcdf files

See: `SCRIPTS/update_database.py`

## Step 7: Grid the Seabird netcdf files into one array

See: `SCRIPTS/stack_mooring_data.py

---

# Tutorial

There is an ipython notebook tutorial of showing the capabilities of the `OceanMooring` and `TimeSeries` objects [here](https://nbviewer.jupyter.org/url/bitbucket.org/mrayson/browse-basin-2010-expt/raw/master/SCRIPTS/tutorial-timeseries.ipynb).
