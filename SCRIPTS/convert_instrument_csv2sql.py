"""
Converts the csv file to an sql database
"""

import os
from soda.dataio import mydbase
import pandas as pd

##########
csvfile = 'DATA/UWA-BB2010-Instruments.csv'
outputdb = 'DATA/UWA-BB2010-Instruments.db'

#########

### Read the csvfile
data = pd.read_csv(csvfile, index_col='Site')

# Convert to a dictionary that can be plugged straight into dict2sql
sites = data['Depth'].keys()

mydb={}
for ii in sites:
   site={}
   for vv in data.keys():
       site.update({vv:data[vv][ii]})
   site.update({'Site':ii})
   mydb.update({ii:site})

mydbase.dict2sql(outputdb, mydb, 'Deployments')
print 'Done'

"""
mydb = {}
for ii in deps.keys():
    fname =  deps[ii]['FileName']
    if fname.shape[0]>0:
	fname = fname[0]
	if filedict.has_key(fname):
	    site = deps[ii]['Site'][0]
	    mydb.update({ii:\
		{
		'FileName':fname,
		'Site':deps[ii]['Site'][0],
		'InstrumentDepth':deps[ii]['InstrumentDepth'][0][0],
		'TimeZone':deps[ii]['TimeZone'][0],
		'FilePath':filedict[fname],
		'Latitude':sites[site]['Latitude'][0],
		'Longitude':sites[site]['Longitude'][0],
		'Depth':sites[site]['Depth'][0],
   		'StationID':ii,
		}
	    })
	else:
	    print 'Cannot find file:\n\t%s!'%fname

mydbase.dict2sql(outputdb,mydb, 'Deployments')
"""
	

