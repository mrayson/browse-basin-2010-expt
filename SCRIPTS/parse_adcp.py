# coding: utf-8

"""
Process RDI adcp data using dolfyn (lkilcher.github.io/dolfyn/)
"""

#from dolfyn.adp import api as adcp
#from dolfyn.adp import rotate
from mycurrents.adcpio import ADCP_io
from soda.dataio import mydbase

import xray
import os
from datetime import datetime, timedelta
from collections import OrderedDict
import numpy as np
from glob import glob

def read_all_adcp(adcpfiles, query, ii, config={}):

    # Get all of the files
    adcps = []
    adcp1 = None
    for ff in sorted(glob(adcpfiles)):
        print ff
        adcp = ADCP_io(ff, rotate=True, config_update=config)
        if adcp1 is None:
            adcp1 = adcp
        else:
            adcp.ds['distance'] = adcp1.ds.distance

        adcps.append(adcp.ds)

    # Concatenate
    alladcp = xray.concat(adcps,dim='time',coords='minimal')

    # Add metadata from the sql query to the attributes
    for kk in query.keys():
        alladcp.attrs.update({kk:query[kk][ii]})
    alladcp.attrs.update({'StationName':query['Site'][ii]})

    return alladcp, adcp.encoding

    #alladcp.to_netcdf(ncfile, mode=mode, encoding=adcp.encoding, group=group)
    #print 'Done'


##########
basedir = '/home/suntans/Share/BrowseFieldData/UWA_ShelfMooring'

#adcpfiles = '%s/DATA/ADCP600kHz/BR600*.000'%basedir
#group='RDI600'
#config={}

#### Can't read the 150 kHz...
#adcpfiles = '%s/DATA/ADCP150kHz/BR150003.000'%basedir
#ncfile = '%s/ProcessedData/UWABB2010_RDIADCPData.nc'%basedir
#group='RDI150'
#config={}

#############
# Inputs

dbfile = '%s/DATA/UWA-BB2010-Instruments.db'%basedir
table = 'Deployments'

condition = 'InstrumentType LIKE "%ADCP%"'

ncfile = '%s/ProcessedData/UWABB2010_RDIADCPData.nc'%basedir

mode='w'
#####

# Query the database and get all of the sites
varnames = ['StationID','FileName','FilePath','Latitude','Longitude','InstrumentDepth',\
	'Depth','Site']
query = mydbase.returnQuery(dbfile, varnames, table, condition)
print 'Found stations:\n', query['StationID']

nfiles = len(query['FileName'])

ii=0
for adcpdir, adcpfiles in zip(query['FilePath'],query['FileName']):
    alladcp, encoding = read_all_adcp('%s/%s'%(adcpdir,adcpfiles), query, ii)

    group = alladcp.Site
    alladcp.to_netcdf(ncfile, mode=mode, encoding=encoding, group=group)
    print 'Done'

    ii+=1
    mode='a'

'''
# Test: read binary and save
adcp = ADCP_io(adcpfile, rotate=True, config_update=config)
adcp2 = ADCP_io(adcpfile2, rotate=True, config_update=config)
print adcp.ds.time.values[0], adcp.ds.time.values[-1]

# Concatenation code
adcp2.ds['distance'] = adcp.ds.distance
adcp3 = xray.concat([adcp.ds,adcp2.ds],dim='time',coords='minimal')
#adcp.to_netcdf(ncfile)
#
### Test: read netcdf
#adcp = ADCP_io(ncfile, rotate=True)
#
'''
