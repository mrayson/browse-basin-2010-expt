"""
Read seabird cnv files 
"""

from mycurrents import seabird as sb
#import soda.utils.mypandas as mpd
from soda.dataio import mydbase

from netCDF4 import num2date
from datetime import datetime,timedelta
import xray
import numpy as np
import os

import pdb


#############
# Inputs

dbfile = 'DATA/UWA-BB2010-Instruments.db'
table = 'Deployments'

#condition = 'StationID LIKE "%SBE56%"'
#condition = 'StationID LIKE "%SBE39%"'
#condition = 'StationID LIKE "%SBE%"'
condition = 'InstrumentType LIKE "%SBE%"'
#condition = 'Site LIKE "%SCR200%"'

outfile = 'ProcessedData/UWABB2010_SeabirdData.nc'
#########

# Query the database and get all of the sites
varnames = ['StationID','FileName','FilePath','Latitude','Longitude','InstrumentDepth',\
	'Depth','Site']
query = mydbase.returnQuery(dbfile, varnames, table, condition)
print 'Found stations:\n', query['StationID']

nfiles = len(query['FileName'])
mode='w'
for ii in range(nfiles):
    filename = '%s/%s'%(query['FilePath'][ii],query['FileName'][ii])

    print filename

    # Get the extension
    fname, ext = os.path.splitext(filename)

    # This returns an xray dataset object
    if ext == '.cnv':
        data = sb.parse_seabird_cnv(filename)
    elif ext == '.asc':
        data = sb.parse_seabird_asc(filename)


    # Fill in some extra info about the station
    #data.attrs.update({'stationname':query['Site'][ii]})
    #data.attrs.update({'original_file':query['FileName'][ii]})
    for vv in varnames:
        data.attrs.update({vv:query[vv][ii]})

    #stationname = '%s_%s_%dm'%(\
    #    query['Site'][ii].split('-')[0],\
    #    query['StationID'][ii].split('-')[0],\
    #    int(query['InstrumentDepth'][ii]),\
    #    )
    stationname = query['Site'][ii]

    data.attrs.update({'StationName':stationname})

    #print data

    # Save
    data.to_netcdf(outfile,group=stationname, mode=mode)
    mode = 'a'

    print '\tWritten to group: %s'%stationname

print 72*'#'
print 'done.'
print 72*'#'
