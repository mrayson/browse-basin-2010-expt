"""
Stack mooring observations together and store
"""

from soda.dataio import netcdfio
from mycurrents import oceanmooring as om

######
# Inputs
######
dbfile = 'BrowseBasin2010_Obs.db'
tablename = 'observations'

stationlike = 'UWA_Browse_100m'
station = 'UWA_Browse_100m_SBE39'

#stationlike = 'UWA_Browse_200m'
#station = 'UWA_Browse_200m_SBE39'

varname = 'temperature'
group = '%s_SBE_T'%stationlike

#varname = 'pressure'
#group = '%s_SBE_P'%station

outfile = 'ProcessedData/UWABB2010_Gridded_Mooring_T.nc'
mode = 'a'
#######

# Load the data
temp = netcdfio.load_sql_ncstation(dbfile, station, varname,\
        otherquery=None)
        #otherquery='StationID LIKE "%%%s%%"'%stationlike)

# Sort by station names 
# THis way the SBE37/9 go first (less memory)
names = [ii.StationName for ii in temp]
sortnames = names.sort()

if len(names) == 0:
    raise Exception, 'could not find any stations.'

for ii, name in enumerate(names):
    print 'Stacking instrument: %s...'%name
    for tt in temp:
     	if tt.StationName == name:
	    if ii == 0:
	        data = om.from_xray(tt)
	    else:
		data.vstack( om.from_xray(tt), clip=True )
            print data.Z

#for ii, tt in enumerate(temp):
#    print tt.StationName
#    if ii == 0:
#        data = om.from_xray(tt)
#    else:
#        data.vstack( om.from_xray(tt), clip=True )


# Change the station name and id
data.StationID = stationlike
data.StationName = station

# Write to output netcdf
data.to_netcdf(outfile, group=group, mode=mode)
print 'Done'

