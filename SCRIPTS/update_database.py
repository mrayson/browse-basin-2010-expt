# -*- coding: utf-8 -*-
"""
Update or create a database with point observation netcdf files
"""

from soda.dataio import netcdfio

####
# Inputs
create = True

basedir = '/home/suntans/Share/BrowseFieldData/UWA_ShelfMooring'

# This could be an existing database
dbfile = '%s/BrowseBasin2010_Obs.db'%basedir
   
ncfiles = [\
    #'%s/WEL/RPS_WEL_BrowseBasin_20062007.nc'%basedir,\
    '%s/ProcessedData/UWABB2010_SeabirdData.nc'%basedir,\
    '%s/ProcessedData/UWABB2010_RDIADCPData.nc'%basedir,\
    ]

# nctype = 3 is my format
nctype = 3
####

if create:
    print 'Creating database: %s'%dbfile
    netcdfio.createObsDB(dbfile)
    
for nc in ncfiles:
    print 'Inserting metadata from: %s'%nc    
    netcdfio.netcdfObs2DB(nc,dbfile, nctype=nctype)

print 'Done.'
