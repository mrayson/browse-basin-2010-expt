function data = Convert_ADCP(filename,theAzimuths,theElevations,theOrientation)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% filename = '../ADCP150kHz/BR150000.000';
% theAzimuths = [270 90 0 180];
% theElevations = [-70 -70 -70 -70];
% theOrientation='up';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data = workhorseParseBeams( {filename} );

% Put velocity, backscatter and correlation into 3D array

nz = length(data.dimensions{2}.data);
nt = length(data.dimensions{1}.data);

vel = zeros(4,nt,nz);
% corr = zeros(4,nt,nz);
absi = zeros(4,nt,nz);
% good = zeros(4,nt,nz);

for n = 1:4
    vel(n,:,:) = data.variables{n}.data;
    absi(n,:,:) = data.variables{n+4}.data; %Backscatter
%     corr(n,:,:) = data.variables{n+10}.data; % Correlation
%     good(n,:,:) = data.variables{n+14}.data; % Good readings
end

% Transform coordinates 
% Needs to be done in for loop
heading =  data.variables{21}.data;
roll =  data.variables{20}.data;
pitch =  data.variables{19}.data;

uvel =zeros(nt,nz);
vvel = zeros(nt,nz);
wvel = zeros(nt,nz);
errvel = zeros(nt,nz);
% bscat = squeeze(mean(absi,1));
bscat = squeeze(max(absi,[],1));
wh=waitbar(0,'Transforming Beam Coordinates...');
for t = 1:nt
    waitbar(t/nt,wh);
        beam = squeeze(vel(:,t,:))';
        enu = bm2geo(beam, theElevations, theAzimuths, ...
								heading(t,1),pitch(t,1), roll(t,1), ...
								theOrientation);
        uvel(t,:) = enu(:,1);
        vvel(t,:) = enu(:,2);
        wvel(t,:) = enu(:,3);
        errvel(t,:) = enu(:,4);
        
end
close(wh);

% outputting to structure array
data.time = data.dimensions{1}.data;
data.heightASB = data.dimensions{2}.data;
data.u = uvel;
data.v = vvel;
data.w = wvel;
data.P = data.variables{10}.data;
data.T = data.variables{9}.data;
data.bscat=bscat;
data.mask = ~isnan(uvel) | ~isnan(vvel) | ~isnan(wvel);

