clear

% filename = '../ADCP150kHz/_RDI_004.000';
filename = '../ADCP600kHz/BR600000.000';

% data = workhorseParse( filename );



data = workhorseParseBeams( {filename} );
ensembles = readWorkhorseEnsembles( filename );
% [adcp,cfg,ens,hdr]=rdradcp(filename,1);

% Put velocity, backscatter and correlation into 3D array

nz = length(data.dimensions{2}.data);
nt = length(data.dimensions{1}.data);

vel = zeros(4,nt,nz);
corr = zeros(4,nt,nz);
absi = zeros(4,nt,nz);
good = zeros(4,nt,nz);

for n = 1:4
    vel(n,:,:) = data.variables{n}.data;
    absi(n,:,:) = data.variables{n+4}.data;
    corr(n,:,:) = data.variables{n+10}.data;
    good(n,:,:) = data.variables{n+14}.data;
end

% Performing QC to find "worst beam"

% Criteria 1) Product of intensity and correlation
QC = corr.*absi;

beam_quality = sum( sum(QC,3),2);

goodbeams = find(beam_quality>min(beam_quality));

velnew = zeros(3,nt,nz);
for n = 1:3
   velnew(n,:,:) = vel(goodbeams(n),:,:);
end

% Transform coordinates 
% Needs to be done in for loop
heading =  data.variables{21}.data;
roll =  data.variables{20}.data;
pitch =  data.variables{19}.data;

uvel =zeros(nt,nz);
vvel = zeros(nt,nz);
wvel = zeros(nt,nz);

wh=waitbar(0,'Transforming Beam Coordinates...');
for t = 1:nt
    waitbar(t/nt,wh);
    for n = 1:nz
        beam = squeeze(velnew(:,t,n));
        [enu]=transform_coordsystem_ADCP(heading(t,1),roll(t,1),pitch(t,1), beam);
        uvel(t,n) = enu(1);
        vvel(t,n) = enu(2);
        wvel(t,n) = enu(3);
    end
end
close(wh);

depth = data.dimensions{2}.data;
time = data.dimensions{1}.data;
time = time-time(1);
figure('position',[308   718   953   231])
subplot(1,3,1)
pcolor(time,depth,uvel');
shading flat
colorbar
title(['Velocity Beam ',num2str(n)])
subplot(1,3,2)
pcolor(time,depth,vvel');
shading flat
colorbar
title(['Backscatter Intensity Beam ',num2str(n)])
subplot(1,3,3)
pcolor(time,depth,wvel');
shading flat
colorbar
title(['Correlation Beam ',num2str(n)])
% pcolor(squeeze(sum(goodind,1)));shading flat
% sum(goodind(:))
% prod([3,nt,nz])


% depth = data.dimensions{2}.data;
% time = data.dimensions{1}.data;
% time = time-time(1);
% for n = 1:4
%     figure('position',[308   718   953   231])
%     subplot(1,3,1)
%     pcolor(time,depth,data.variables{n}.data');
%     shading flat 
%     colorbar
%     title(['Velocity Beam ',num2str(n)])
%     subplot(1,3,2)
%     pcolor(time,depth,data.variables{4+n}.data');
%     shading flat 
%     colorbar
%     title(['Backscatter Intensity Beam ',num2str(n)])
%      subplot(1,3,3)
%     pcolor(time,depth,data.variables{10+n}.data');
%     shading flat 
%     colorbar
%     title(['Correlation Beam ',num2str(n)])
% end

% depth = data.dimensions{2}.data;
% time = data.dimensions{1}.data;
% time = time-time(1);
% 
% nvar = length(data.variables);
% for n = 1:nvar
%     figure
%     if length(data.variables{n}.dimensions)==2
%         pcolor(time,depth,data.variables{n}.data');
%         shading flat
%         colorbar
%     elseif length(data.variables{n}.dimensions)==1
%         plot(time,data.variables{n}.data)
%     end
%     title([data.variables{n}.name])
%     pause();
%     close(gcf)
% end

% data = readWorkhorseEnsembles( filename );
