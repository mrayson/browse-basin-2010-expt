clear

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datadir = '../SBE39/';
xlsfile = '../InstrumentLayout.xlsx';

site = 'UWA_Browse_100m';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Read in the instrument layout from spreadsheet
[NUMERIC,TXT,xldata]=xlsread(xlsfile,'sheet1');

[npts,c] = size(xldata);

ctr=0;
for n = 2:npts
    if strcmp(xldata{n,1},site)
        if strcmp(xldata{n,5},'SBE 39') || strcmp(xldata{n,5},'SBE 39 TP' )
            ctr=ctr+1;
            filename = xldata{n,7};
            fprintf('Retrieving %s data from file: %s \n',xldata{n,5},xldata{n,7});
            % Read in the instrument data
            indata = SBE3x({[datadir,filename]});
        elseif strcmp(xldata{n,5},'SBE 37 CT')
%             ctr=ctr+1;
%             fprintf('Retrieving %s data from file: %s \n',xldata{n,5},xldata{n,7});
%             filename = xldata{n,7};
%             indata = read_sbe37([datadir,filename]);
        end
        
        nvars = length(indata.variables);
        
        data(ctr).site = xldata{n,1};
        data(ctr).instrument_type = xldata{n,5};
        data(ctr).serial_no = xldata{n,6};
        data(ctr).lon = xldata{n,3};
        data(ctr).lat = xldata{n,4};
        data(ctr).total_depth = xldata{n,2};
        data(ctr).instrument_height = xldata{n,8};
        data(ctr).TIME = indata.dimensions{1}.data;
        
        for k = 1:nvars
            var = indata.variables{k}.name;
            eval(['data(ctr).',var,'=indata.variables{k}.data;'])
        end
        
        
    end
end

save(['../matfiles/',site,'_temp.mat'],'data')

% data = SBE3x({'../SBE39/SBE39_3719.asc'});
% data2 = SBE3x(filename2);
% 
% plot(data.dimensions{1}.data,data.variables{2}.data)
% hold on
% plot(data2.dimensions{1}.data,data2.variables{2}.data,'r')
% legend('5082','5083')