clear

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
datadir = '../ADCP600kHz/';
theAzimuths = [270 90 0.01 180];
theElevations = [-70 -70 -70 -70];
theOrientation='up';

lon = 121.83053; % 100 m (600 kHz)
lat = -15.03134;

% lon = 121.70669; % 200 m (150 kHz)
% lat = -14.9556; 

outfile = 'O:\Browse Field Data\UWA_ShelfMooring\matfiles\UWA_Browse_100m_ADCP.mat';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% find all the ADCP files in the directory
D = dir(datadir);
ctr=1;
for n = 1:length(D)
   if length(D(n).name) > 2 
       if strcmp(D(n).name(end-2:end),'000')
           file{ctr}=D(n).name;
           ctr=ctr+1;
       end
   end
           
end
tic
data.time = []; data.u=[]; data.v=[]; data.w=[]; data.P=[];data.T=[];data.mask=[];data.bscat=[];
for n = 1:length(file);
    fprintf('Reading file: %s ...\n',file{n});
    tempdata = Convert_ADCP([datadir,file{n}],theAzimuths,theElevations,theOrientation);
    data.heightASB = tempdata.heightASB;
    data.lon=lon;
    data.lat=lat;
    data.time=[data.time;tempdata.time];
    data.u=[data.u;tempdata.u];
    data.v=[data.v;tempdata.v];
    data.w=[data.w;tempdata.w];
    data.P=[data.P;tempdata.P];
    data.T=[data.T;tempdata.T];
    data.mask=[data.mask;tempdata.mask];
    data.bscat=[data.bscat;tempdata.bscat];
end
toc
save(outfile,'data')