% function PlotTempMovie(outfile,moviefile)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
tempfile = '../matfiles/UWA_Browse_100m_temp_processed.mat';
adcpfile = '../matfiles/UWA_Browse_100m_adcp.mat';
reftime = datenum(2010,1,1);
tstep = 3/24; % time step in days
twindow(1) = 2; % time window in days
twindow(2) = 0.25; % time window in days
axlims = [-100,-20];
clims = [-0.8,0.8];

shelfang=32.3; % angle between two moorings = -32.34 degrees

timestart = datenum(2010,3,29,3,0,0);
timeend = datenum(2010,3,29,8,0,0);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(adcpfile)
adcp=data;clear data
load(tempfile)


depth = -data.totaldepth+data.tempdepth;
adcpdepth = -data.totaldepth+flipud(adcp.heightASB);
% time = data.time-reftime;
% adcptime = adcp.time-reftime;
time = (data.time-timestart)*24;
adcptime = (adcp.time-timestart)*24;

t = data.time(1,1);

% rotate adcp velocities to shelf
[ucross,valong] = roms_vector_rotate(adcp.u,adcp.v,shelfang*pi/180);

% figure
% pcolor(adcp.time-reftime,adcp.heightASB,adcp.u');
% shading flat
% colorbar


figure
subplot(2,1,1)
t1= find(data.time>=timestart,1,'first'); % thermistor time
t2 = find(data.time>=timeend,1,'first');
ta1= find(adcp.time>=timestart,1,'first'); % adcp time
ta2 = find(adcp.time>=timeend,1,'first');
h(1)=pcolor(adcptime(ta1:ta2,1),adcpdepth,ucross(ta1:ta2,:)');
hold on
[c,h1(1)] = contour(time(t1:t2,1),depth,data.temp(t1:t2,:)',[0:35],'k');
shading interp
set(gca,'ylim',axlims);
% set(gca,'xlim',[timestart,timeend]);
caxis(clims)
colorbar
title(['Time: ',datestr(data.time(t1))])
ylabel('Depth [m]')

subplot(2,1,2)
t1= find(data.time>=timestart,1,'first'); % thermistor time
t2 = find(data.time>=timeend,1,'first');
ta1= find(adcp.time>=timestart,1,'first'); % adcp time
ta2 = find(adcp.time>=timeend,1,'first');
h(1)=pcolor(adcptime(ta1:ta2,1),adcpdepth,adcp.bscat(ta1:ta2,:)');
hold on
[c,h1(1)] = contour(time(t1:t2,1),depth,data.temp(t1:t2,:)',[0:35],'k');
shading interp
set(gca,'ylim',axlims);
% set(gca,'xlim',[timestart,timeend]);
caxis([60,90])
colorbar
xlabel('Time [hr]')
ylabel('Depth [m]')
    
