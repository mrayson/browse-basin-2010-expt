% function PlotTempMovie(outfile,moviefile)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
tempfile = '../matfiles/UWA_Browse_100m_temp_processed.mat';
adcpfile = '../matfiles/UWA_Browse_100m_adcp.mat';
moviefile = '../movies/UWA_100m_uv_temp';
reftime = datenum(2010,1,1);
tstep = 3/24; % time step in days
twindow(1) = 2; % time window in days
twindow(2) = 0.25; % time window in days
axlims = [-100,0];
clims = [-0.8,0.8];

shelfang=32.3; % angle between two moorings = -32.34 degrees

outimage = '../images/browse_100m_uv_temp';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load(adcpfile)
adcp=data;clear data
load(tempfile)


depth = -data.totaldepth+data.tempdepth;
adcpdepth = -data.totaldepth+flipud(adcp.heightASB);
time = data.time-reftime;
adcptime = adcp.time-reftime;

t = data.time(1,1);

% rotate adcp velocities to shelf
[ucross,valong] = roms_vector_rotate(adcp.u,adcp.v,shelfang*pi/180);

% figure
% pcolor(adcp.time-reftime,adcp.heightASB,adcp.u');
% shading flat
% colorbar


figure('position',[328 65 1015 882])
ctr=0;
maxctr=168;
movienum=1;
while t < data.time(end,1)
    if ctr == 0
        makeqtmovie('start',[moviefile,num2str(movienum),'.mov']);
        MakeQTMovie('quality',1.0);
        movienum=movienum+1;
    end
    ctr=ctr+1;
        
    for n = 1:2
        subplot(2,1,n)
        t1= find(data.time>=t-twindow(n)/2,1,'first');
        t2 = find(data.time>=t+twindow(n)/2,1,'first');
        ta1= find(adcp.time>=t-twindow(n)/2,1,'first');
        ta2 = find(adcp.time>=t+twindow(n)/2,1,'first');
        h(n)=pcolor(adcptime(ta1:ta2,1),adcpdepth,ucross(ta1:ta2,:)');
        hold on
        [c,h1(n)] = contour(time(t1:t2,1),depth,data.temp(t1:t2,:)',[0:35],'k');
        shading interp
        set(gca,'ylim',axlims);
        set(gca,'xlim',[t-reftime-twindow(n)/2,t-reftime+twindow(n)/2]);
        caxis(clims)
        colorbar
        
        % plot locations of subplot
        if n ~= 2
           x1 =  t-reftime-twindow(n+1)/2;
           x2 =  t-reftime+twindow(n+1)/2;
           h2(n) = plot([x1,x1,x2,x2],[axlims(1),axlims(2),axlims(2),axlims(1)],'c--','linewidth',1.5);
        end
        if n == 1
            title(['Time: ',datestr(t)])
        end
        
    end    
    
    outfile=sprintf('%s%03d.png',outimage,t);
    screen2png([outfile],'-r90');
%     A = imread([outfile]);
%     makeqtmovie('addmatrix',A);
    makeqtmovie('addfigure');
    
    pause(0.1)
    delete(h); delete(h1); delete(h2);
    
    t=t+tstep;
    if ctr==maxctr
        MakeQTMovie('framerate',6);
        MakeQTMovie('finish');
        ctr=0;
    end
        
end



