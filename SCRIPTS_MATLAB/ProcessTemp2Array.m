clear

datafile = '../matfiles/UWA_Browse_100m_temp.mat';
outfile = '../matfiles/UWA_Browse_100m_temp_processed.mat';
load(datafile)
% setting all of the arrays onto the same time domain (truncate if
% necessary)
numpoints = length(data);

mintime=0;maxtime=10^10;
for n =1:numpoints
    mintime=max([data(n).TIME(1,1);mintime]);
    maxtime=min([data(n).TIME(end,1);maxtime]);
end


for n =1:numpoints
%     start_ind = find(data(n).TIME>=mintime-(1/(60*24*1.5))&data(n).TIME<=mintime+(1/(60*24*1.5)));    
%     end_ind = find(data(n).TIME>=maxtime-(1/(60*24*1.5))&data(n).TIME<=maxtime+(1/(60*24*1.5)));
    start_ind = find(data(n).TIME>=mintime,1,'first');
    end_ind = find(data(n).TIME>=maxtime,1,'first');
    
    end_ind-start_ind+1;
    
    outdata.totaldepth = data(n).total_depth;
    outdata.lon = data(n).lon;
    outdata.lat = data(n).lat;
    
    outdata.temp(:,n) = data(n).TEMP(start_ind:end_ind,1); 
    outdata.tempdepth(n) = data(n).instrument_height;
    outdata.time = data(n).TIME(start_ind:end_ind,1);
    
    if ~isempty(data(n).PRES)
        outdata.pres = data(n).PRES(start_ind:end_ind,1); 
        outdata.presdepth(n) = data(n).instrument_height;
    end
    
    if exist('data(n).COND','var') && ~isempty(data(n).COND)
        outdata.cond = data(n).COND(start_ind:end_ind,1); 
        outdata.conddepth(n) = data(n).instrument_height;
    end    
    
end

clear data
data=outdata; clear outdata
save(outfile,'data');