function [data] = read_sbe37(filename)

% clear
%
% filename = '../SBE39/SBE37_4567.cap';
%

% Read in file data
[T,C,ddmmyy,hhmm] = textread(filename,'%n%n%s%s','headerlines',41,'delimiter',',');

% Convert time dimension
% This crashes sometimes...
timestr = strcat(ddmmyy,hhmm); clear ddmmyy hhmm
time = datenum(timestr,'dd mmm yyyyHH:MM:SS');

% This takes forever...
% ntime = length(ddmmyy);
% time = zeros(ntime,1);
% for n = 1:ntime
%    time(n,1) = datenum(strcat(ddmmyy{n,1},{' '},hhmm{n,1}),'dd mmm yyyy HH:MM:SS');
% end

data(1).dimensions{1}.name='TIME';
data(1).dimensions{1}.data=time;
data(1).variables{1}.name='TEMP';
data(1).variables{1}.data=T;
data(1).variables{1}.name='COND';
data(1).variables{1}.data=C;